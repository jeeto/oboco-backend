package com.gitlab.jeeto.oboco.common.archive;

public interface ArchiveReaderEntry {
	public String getName();
	public boolean isDirectory();
}
